// creating a buffer

var buf = new Buffer.from("Hello World!")
var buf1 = new Buffer.from("8b76fde713ce", 'base64')
// create a new buffer by specifying it length

var buf2 = new Buffer.alloc(1024)
console.log(buf2[10])
console.log(buf2.length)
console.log(buf)
console.log(buf1)

// Slicing a buffer
var buffer = new Buffer.from("this is the content of my buffer")
var smallerBuffer = buffer.slice(8, 19)
console.log(smallerBuffer.toString())
buffer[10] = 'c'
console.log(smallerBuffer.toString())

// Copying a buffer
var buffer1 = new Buffer.from("this is the content of my buffer")
var buffer2 = new Buffer.alloc(11)
buffer1.copy(buffer2, 0, 8, 19)
console.log(buffer2.toString())

// Decoding a buffer
// buffer can be converted into a UTF-8-encoded using toString() method
console.log(buffer1.toString("base64"))