const fs = require('fs')
const path =
    "/home/nandigamchandu/practice/web/javascript/nodePractice/chapter9/sample.txt";

require('http').createServer((req, res)=>{
    var rs = fs.createReadStream(path)
    rs.pipe(res, {end: false})
    rs.on('end', ()=>{
        res.write("And that's all, folks!")
        res.end()
    })
}).listen(8080)