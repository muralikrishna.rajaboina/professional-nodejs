const express = require("express")
const router = express.Router()
const Task = require("../data/models/task")
const async = require('async')
const auth = require("./middleware/auth")

router.get("/tasks", auth.required, (req, res, next)=>{
    var page = (req.query.page && parseInt(req.query.page)) || 0;
    async.parallel(
        [
            next =>{
                Task.find().estimatedDocumentCount(next)
            },
            next => {
                Task.find({})
                    .sort({createdAt: -1})
                    .skip(page * 3)
                    .limit(3)
                    .exec(next)
            }
        ],
        (err, results)=>{
            if(err){
                return next(err)
            }
            const count = results[0]
            const tasks = results[1]

            const lastPage = (page + 1) * 3 >= count
            res.json({
                title: "Task",
                tasks: tasks,
                page: page,
                lastPage: lastPage,
            })
        }
    )
})

router.post("/create/task", auth.required, (req, res, next)=>{
    const task = new Task(req.body)
    task.save().then(()=>{
        res.send('task is created')
    })
})

router.delete("/delete/task/:id", auth.required, (req, res, next)=>{
    // console.log()
    Task.findOneAndDelete({id : req.param.id}, (err, task)=>{
        if(err){
            return res.send("Not found", 404)
        }

        res.send(`Task has been deleted ${task}`)
    
    })
})

module.exports = router