var express = require("express");
var router = express.Router();
var notLoggedIn = require("./middleware/not_logged_in");
var loadUser = require("./middleware/load_user");
var User = require("../data/models/user");
var async = require("async");
const auth = require("./middleware/auth")

router.get("/users", auth.required, (req, res, next) => {
  var page = (req.query.page && parseInt(req.query.page)) || 0;
  async.parallel(
    [
      next => {
        User.find().estimatedDocumentCount(next);
      },
      next => {
        User.find({})
          .sort({ name: 1 })
          .skip(page * 3)
          .limit(3)
          .exec(next);
      },
    ],
    (err, results) => {
      if (err) {
        return next(err);
      }

      var count = results[0];
      var users = results[1];

      var lastPage = (page + 1) * 3 >= count;
      res.json({
        title: "Users",
        users: users,
        page: page,
        lastPage: lastPage
      })
    }
  );
});

router.get("/users/new", (req, res) => {
  res.render("users/new", { title: "New User" });
});

router.get("/users/:name", loadUser, function(req, res, next) {
  res.render("users/profile", { title: "User profile", user: req.user });
});

router.post("/create/user",auth.optional,(req, res, next) => {
  console.log(req.body)
  const finalUser = new User(req.body)
  finalUser.setPassword(req.body.password);
  finalUser.save().then(()=>{
    res.send('user is created')
  })
});

router.delete("/users/:name",auth.required, loadUser, (req, res, next) => {
  req.user.remove(err => {
    if (err) {
      return next(err);
    }
    res.redirect("/users");
  });
});



module.exports = router;