var spawn = require('child_process').spawn

const node = "/home/nandigamchandu/.nvm/versions/node/v11.12.0/bin/node";
var child = spawn(`${node}`, ['06_plus_one.js'])

setInterval(()=> {
    var number = Math.floor(Math.random() * 10000)
    child.stdin.write(number + "\n")
    child.stdout.once('data', (data)=>{
        console.log('child replied to ' + number + ' with:' + data)
    })
}, 1000);

child.stderr.on('data', (data)=>{
    process.stdout.write(data)
})