var express = require("express");
var router = express.Router();
var auth = require("./auth");
var Users = require("../data/models/user");
var passport = require('passport')
var t = require('tcomb-validation')
var validation = t.validate


const User = t.struct({
  email: t.String,
  password: t.String
})
const logIn = t.struct({
  user: User
})

router.post("/", auth.optional, (req, res, next) => {
  if(!validation(req.body, logIn).isValid()){
    res.end(validation(req.body, logIn).firstError().message);
  }
  const {
    body: { user }
  } = req;
  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required"
      }
    });
  }

  var finalUser = new Users(user);
  finalUser.setPassword(user.password)

  return finalUser.save().then(()=> res.json({user: finalUser.toAuthJSON()}))
});


router.post("/login", auth.optional, (req, res, next) => {
  if (!validation(req.body, logIn).isValid()) {
    return res.end(validation(req.body, logIn).firstError().message);
  }
  return passport.authenticate("local", (err, passportUser, info)=>{
    if(err){return next(err)}
    if(passportUser){
      const user = passportUser
      user.token = passportUser.generateJWT()
      return res.json({user : user.toAuthJSON()})
    }
    return res.status(400).send(info);

  })(req, res, next);
});


router.get("/current", auth.optional, (req, res, next) => {
  if (!req.payload){
    return res.status(400).send({error: "token is required"})
  }
  const {payload: { id }} = req;
  console.log(id)
  return Users.findById(id).then(user => {
    if (!user) {
      return res.sendStatus(400);
    }
    return res.json({ user: user.toAuthJSON() });
  });
});

module.exports = router;
