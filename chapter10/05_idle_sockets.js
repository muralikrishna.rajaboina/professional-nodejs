require('net').createServer((socket) => {
    socket.setTimeout(10000)
    socket.on('data', (data)=>{console.log(data)})
    socket.on('timeout', ()=>{
        socket.write('idle timeout, disconnecting, bye!')
        socket.end()
    })
}).listen(4001)