const fs = require('fs')
const http = require('http')
const EventEmitter = require('events').EventEmitter
const util = require('util')
// continuation- passing style
// publisher/subscribe pattern
// Understanding the standard callback pattern
fs.readFile('/home/nandigamchandu/house.txt', function(err, fileContent){
    if(err){
        throw err
    }
    console.log('file content', fileContent.toString())
})
console.log("Hi Chandu")

// Understanding the event emitter pattern
// separate the event emitter and the event listeners.

// var req = http.request(options, function(response){
//     response.on("data", function(data){
//         console.log("some data from the response", data)
//     })
//     response.on("end", function(){
//         console.log("response ended")
//     })
// })
// req.end()

// Inheriting from Node Emitter
var MyClass = function(){
}

util.inherits(MyClass, EventEmitter)

MyClass.prototype.someMethod = function(){
    this.emit("custom event", "argument_1", "argument_2")
}

var myInstance = new MyClass()
myInstance.on('custom event', function(str1, str2){
    console.log(`========got a custom event with the ${str1} and ${str2}==============`)
})
myInstance.someMethod()

var Ticker = function(){
    var self = this
    setInterval(function(){
        self.emit('tick')
    }, 1000)
}

util.inherits(Ticker, EventEmitter)

var ticker = new Ticker()
ticker.on('tick', function(){
    console.log('tick')
})

// ticker.once('tick', function(){
//     console.log('tick')
// })