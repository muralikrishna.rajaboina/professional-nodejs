var connect = require('connect')
var cookieParser = require("cookie-parser");
var app = connect()

app.use(cookieParser())
app.use((req, res) => {
    return res.end(JSON.stringify(req.cookies));
})

app.listen(8080)