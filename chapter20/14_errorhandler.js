var connect = require("connect");
var errorhandler = require("errorhandler")
var notifier = require("node-notifier")

const errorNotification = (err, str, req) =>{
    var title = 'Error in' + req.method + ' ' + req.url
    notifier.notify({title: title,
    message: str})
}

var errorCreator = require("./09_create_error");
var saveRequest = require("./07_save_request");
var writeHeader = require("./05_write_header");
var replyText = require("./03_reply_text");

var app = connect();
app.use(errorCreator());
app.use(writeHeader("X-Powered-By", "Node"));
app.use(replyText("Hello World"));
app.use(errorhandler({log : errorNotification}));

app.listen(8080);
