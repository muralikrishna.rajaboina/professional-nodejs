GET /
{ host: 'localhost:8080',
  connection: 'keep-alive',
  pragma: 'no-cache',
  'cache-control': 'no-cache',
  'upgrade-insecure-requests': '1',
  'user-agent':
   'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
  accept:
   'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
  cookie:
   'PLAY_SESSION=eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImNzcmZUb2tlbiI6IjQzZGYwZjg2MjA1OTQ4NmMzMWExMzU3ODBmMzM3Yzk2YTNkYzA1NjktMTU1MjAxOTMwMTIwNS00OTQ0ODZjNGVkZGUwNmY1NWEwMjhiNDQifSwibmJmIjoxNTUyMDE5MzAxLCJpYXQiOjE1NTIwMTkzMDF9.tUQVNvZ8G-H5dFX7Q8nbRolVe97PIGKn-KtD_4F0ylU; username-localhost-8888="2|1:0|10:1554786950|23:username-localhost-8888|44:OGY5ZWY5ODM3ODA5NDAxNzg0NWQ3ZjZiYmE2NDI2N2M=|90c954d6e8169aeedf1d22e3a87ef1782052c2be5e91477ebc559506bcdb2819"; username-localhost-8889="2|1:0|10:1554812042|23:username-localhost-8889|44:YjRjY2QwNDVhYmUyNGFiMGExYzgxMTBmYzg5MjBlODg=|8fa0326f1af50cf8ce65f1e8549afdd9325732aacc64b145633afd3453a6f838"' }
