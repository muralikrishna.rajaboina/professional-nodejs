var connect = require('connect')
var query = require("connect-query")
var cookieParser = require("cookie-parser")
var cookieSession = require("cookie-session")
var format = require('util').format

var app = connect()
app.use(query())
app.use(cookieParser('this is my secret string'))

app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']}))

app.use((req, res)=>{
    for (var name in req.query){
        req.session[name] = req.query[name]
    }

    res.end(format(req.session) + '\n')

})

app.listen(8080)