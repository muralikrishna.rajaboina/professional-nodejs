var connect = require('connect')
var query = require("connect-query");
var app = connect()

app.use(query())
app.use((req, res)=>{
    return res.end(JSON.stringify(req.query))
})

app.listen(8080)