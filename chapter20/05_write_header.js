const writeHeader = (name, value)=>{
    return (req, res, next)=>{
        res.setHeader(name, value)
        next()
    }
}

module.exports = writeHeader