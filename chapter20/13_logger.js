var connect = require("connect");
var morgan = require("morgan");

var writeHeader = require("./05_write_header");
var replyText = require("./03_reply_text");

var app = connect();
app.use(
    morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"')
);
app.use(writeHeader("X-Powered-By", "Node"));
app.use(replyText("Hello World"));

app.listen(8080);
