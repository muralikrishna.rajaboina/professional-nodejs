var serveStatic = require("serve-static")
var connect = require("connect");


var writeHeader = require("./05_write_header");
var replyText = require("./03_reply_text");

var app = connect();
app.use(writeHeader("X-Powered-By", "Node"));
app.use(serveStatic('public'))
app.use(replyText("Hello World"));
app.listen(8080);
