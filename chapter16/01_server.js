var fs = require('fs')
var https = require('https')

var options = {
    key: fs.readFileSync('server_key.pem'),
    cert: fs.readFileSync('server_cert.pem'),
    requestCert: false,
    rejectUnauthorized: false
}

var server = https.createServer(options, (req, res)=>{
    console.log('authorized:', req.socket.authorized);
    console.log('client certificate:', req.socket.getPeerCertificate());
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.end('Hello World')
})

server.listen(4001,'0.0.0.0', ()=>{
    console.log('Server is listening on port', server.address().port)
})