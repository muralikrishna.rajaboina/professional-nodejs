var fs = require('fs')
var https = require('https')

var options = {
    host: '0.0.0.0',
    port: 4001,
    method:'GET',
    path:'/'
}

var request = https.request(options, (res)=>{
    console.log('response.statusCode:', res.statusCode)

    res.on('data', (data)=>{
        console.log('got some data back from the server: ', data)
    })
})

request.write('Hey!\n')
request.end()