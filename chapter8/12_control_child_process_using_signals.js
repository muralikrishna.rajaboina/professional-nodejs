var spawn = require('child_process').spawn
var child = spawn('sleep', ['10'])
setTimeout(()=>{
    child.kill('SIGUSR2')
}, 1000)

process.on('SIGUSR2', ()=>{
    console.log('Got a SIGUSR2 signal')
})
