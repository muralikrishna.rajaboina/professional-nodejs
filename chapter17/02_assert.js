var assert = require('assert')

// test if a value is truthy
var a = true
assert(a, 'a should be truthy')

var b = true
assert.ok(b)

var a = 10
// assert.equal is shallow equality similar to "=="
assert.equal(a, 10, "a should be 10")
assert.equal("10", 10)
assert.notEqual(11, 10)

// assert.strictEqual is shallow equality similar to "==="
assert.strictEqual(10, 10)

// deep equality
var obj = {b: 1, a:[1, 2, 3, 4, 5]}
assert.deepEqual(obj, {a:[1, 2, 3, 4, 5], b:1})