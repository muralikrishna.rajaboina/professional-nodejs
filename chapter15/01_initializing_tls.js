var tls = require("tls");
var fs = require("fs");
var port = 4001;
var clients = [];
var options = {
  key: fs.readFileSync("my_key.pem"),
  cert: fs.readFileSync("my_cert.pem"),
  ca: [fs.readFileSync("client_cert.pem"), fs.readFileSync("my_cert.pem")],
  requestCert: false,
  rejectUnauthorized: false
};

function distribute(from, data) {
  var socket = from.socket;
  clients.forEach(client => {
    if (client !== from) {
      client.write(
        socket.remoteAddress + ":" + socket.remotePort + " said: " + data
      );
    }
  });
}

var server = tls.createServer(options, function(client) {
  clients.push(client);
  client.on("data", function(data) {
    distribute(client, data);
  });

  client.on("close", () => {
    console.log("closed connection");
    clients.splice(clients.indexOf(client), 1);
  });
});

server.listen(port, () => {
  console.log("listening on port", server.address().port);
});
