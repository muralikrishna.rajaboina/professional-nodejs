const myModule2 = require('./myModule2')
// loading module from core
const http = require('http')
// loading module from floder
const myModule = require('./myModuleDir')
// Caching Module
var myModuleInstance1 = require("./my_module")
var myModuleInstance2 = require("./my_module")


myModule2.printA()
myModule2.printB()
myModule2.printC()
console.log(myModule2.pi)
myModule.packageModule()