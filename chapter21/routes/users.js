var express = require('express');
var router = express.Router();
var users = require('../data/users')
var notLoggedIn = require("./middleware/not_logged_in");
var loadUser = require('./middleware/load_user')
var restrictUserToSelf = require('./middleware/restrictUserToSelf')



/* GET users listing. */
router.get('/users', (req, res)=>{
  res.render('users/index', {title: 'Users', users: users})
})

router.get('/users/new', notLoggedIn, (req, res)=>{
  res.render('users/new', {title:"New User"})
})

router.get('/users/:name', loadUser, function(req, res, next) {
  var user = users[req.params.name]
  if (user){
    res.render('users/profile', {title: 'User profile', user: user})
  }else{
    next()
  }
});

router.post('/users', notLoggedIn, (req, res)=>{
  if(users[req.body.username]){
    res.send('Conflict', 409)
  }else{
    users[req.body.username] = req.body
    res.redirect('/users')
  }
})

router.delete("/users/:name", loadUser,restrictUserToSelf, (req, res, next) => {
  if (users[req.params.name]) {
    delete users[req.params.name];
    res.redirect("/users");
  } else {
    next();
  }
});
module.exports = router;
