const request = require("supertest");
const mocha = require("mocha");
const describe = mocha.describe;
const assert = require("chai").assert;
const expect = require("chai").expect;

describe("POST /login", () => {
  it("responses with json", done => {
    request("http://localhost:8080")
      .post("/login")
      .send({
        user: {
          email: "nandigamchandu@gmail.com",
          password: "test"
        }
      })
      .set("Accept", "application/json")
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        assert.equal(res.body.user.email, "nandigamchandu@gmail.com");
        return done();
      });
  });
});

describe("POST /login", () => {
  it("email or password is invalid", done => {
    request("http://localhost:8080")
      .post("/login")
      .send({
        user: {
          email: "nandigamchandu1@gmail.com",
          password: "test"
        }
      })
      .set("Accept", "application/json")
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        expect(res.body).to.be.deep.equal({
          errors: {
            "email or password": "is invalid"
          }
        });
        return done();
      });
  });
});


