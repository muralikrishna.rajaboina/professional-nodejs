const app = require('../server')
const request = require('supertest')
const mocha = require('mocha')
const describe = mocha.describe

describe('GET /user', function(){
    it('responds with json', function(done){
        request(app)
            .get('/user')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done)
    })
})