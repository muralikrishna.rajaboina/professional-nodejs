const request = require("supertest");
const mocha = require("mocha");
const describe = mocha.describe;
const assert = require("chai").assert;
const expect = require("chai").expect;

describe('GET /current', ()=>{
    it('should require authorization', (done)=>{
        request("http://localhost:8080").get('/current').expect(400, done)

    })

    var auth = {}
    before(loginUser(auth))
    it('get current user', (done)=>{
        request("http://localhost:8080")
          .get("/current")
          .set("Authorization", "Token " + auth.token)
          .expect(200, done);

    })


})

function loginUser(auth){
    return function(done){
        request("http://localhost:8080")
            .post("/login")
            .send({
                user: {
                    email: "nandigamchandu@gmail.com",
                    password: "test"
                }
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                auth.token = res.body.user.token
                return done()})}}

