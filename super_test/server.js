const express = require('express')
const app = express()

app.get('/', (req, res)=>{
    res.send('Hello world')
})

app.get('/user', (req, res)=>{
    res.status(200).json({name: 'chandu'})
})

app.listen(3000, ()=>{
    console.log('server ready')
})

module.exports = app