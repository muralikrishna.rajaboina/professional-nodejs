const t = require('tcomb-validation')
const validate = t.validate

let result = validate(1, t.String)
console.log(result.isValid())
console.log(result.firstError().message)


// validating Primitive

console.log("null and undefined validation")
console.log(validate('a', t.Nil).isValid())
console.log(validate(null, t.Nil).isValid())
console.log(validate(undefined, t.Nil).isValid())

// strings
console.log("string validation")
console.log(validate(1, t.String).isValid())
console.log(validate("a", t.String).isValid())

// number
console.log("number validation")
console.log(validate('a', t.Number).isValid())
console.log(validate(1, t.Number).isValid())

// booleans
console.log("validate boolean")
console.log(validate(1, t.Boolean).isValid())
console.log(validate(true, t.Boolean).isValid())

// optional values
console.log("validate optional")
console.log(validate(null, t.maybe(t.String)).isValid())
console.log(validate('a', t.maybe(t.String)).isValid())
console.log(validate(1, t.maybe(t.String)).isValid())

// functions
console.log("validate Function")
console.log(validate(1, t.Function).isValid())
console.log(validate(function(){}, t.Function).isValid())

// dates
console.log("validate date")
console.log(validate(1, t.Date).isValid())
console.log(validate(new Date(), t.Date).isValid())

// regexps
console.log("validate regexps")
console.log(validate(1, t.RegExp).isValid())
console.log(validate(/^a/, t.RegExp).isValid())

// Refinements
const predicate = (x)=>{
    return x >= 0}

// a positive number
console.log("using refinements")
const Positive = t.refinement(t.Number, predicate)
console.log(validate(-1, Positive).isValid())
console.log(validate(1, Positive).isValid())

// Object
console.log("using Structs")
var Point = t.struct({x: t.Number,
y: t.Number})

console.log(validate(null, Point).isValid())
console.log(validate({ x: 0 }, Point).isValid())
console.log(validate({ x: 0 }, Point).firstError().message)
console.log(validate({ x: 0 , y:"a"}, Point).firstError().message)
console.log(validate({ x: 0 , y:1}, Point).isValid())
console.log(validate({ x: 0 , y:1, z: 6}, Point).isValid())
console.log(validate({ x: 0 , y:1, z: 6}, Point, {strict: true}).firstError().message)

// Lists and tuples
console.log("validate List")
let Words = t.list(t.String)
console.log(validate(null, Words).isValid())
console.log(validate(['hello', 1], Words).isValid())
console.log(validate(['hello', "world"], Words).isValid())

console.log("validate Tuple")
var Size = t.tuple([Positive, Positive])
console.log(validate([1], Size).isValid())
console.log(validate([1, -1], Size).isValid())
console.log(validate([1, 2], Size).isValid())

// Enums
console.log("validate Enums")
var CssTextAlign = t.enums.of('left right center justify')
console.log(validate('bottom', CssTextAlign).isValid())
console.log(validate('left', CssTextAlign).isValid())

// Unions
console.log("validation using union" )
var CssLineHeight = t.union([t.Number, t.String])
console.log(validate(null, CssLineHeight).isValid())
console.log(validate(1.4, CssLineHeight).isValid())
console.log(validate('1.2em', CssLineHeight).isValid())

// Dicts
console.log("validation using Dicts")
var Country = t.enums.of(['IT', 'US'], 'Country')
var Warranty = t.dict(Country, t.Number, "Warranty")
console.log(validate(null, Warranty).isValid())
console.log(validate({a: 2}, Warranty).isValid())
console.log(validate({US: 2, IT:'a'}, Warranty).isValid())
console.log(validate({US: 2, IT:1}, Warranty).isValid())

// Intersections
console.log("validation using Intersections")
var Min = t.refinement(t.String, (s)=> s.length > 2, "Min")
var Max = t.refinement(t.String, (s)=> s.length < 5, "Max")
var MinMax = t.intersection([Min, Max], 'MinMax')

console.log(MinMax.is('abc'))
console.log(MinMax.is('a'))
console.log(MinMax.is('agdhgjs'))

// Nested structures
var Post = t.struct({
    title: t.String,
    content: t.String,
    tags: Words
})

var mypost = {
    title: "Awesome!",
    content: "You can validate structures with arbitrary level of nesting",
    tags: ['validation', 1]
}

console.log(validate(mypost, Post).isValid())
console.log(validate(mypost, Post).firstError().message)

// Customise error messages
var ShortString = t.refinement(t.String, (s)=>{
    return s.length < 3
})

ShortString.getValidationErrorMessage = function(value){
    if(!value){
        return 'Required'
    }
    if(value.length >= 3){
        return 'Too long my friend'
    }
}

console.log(validate('abc', ShortString).firstError().message)

// Use cases
//  JSON schema
var Schema = t.struct({
    foo: t.Number,
    bar: t.enums.of('a b c')
})

var json = {
    "foo": "this is a string, not a number",
    "bar": "this is a string that isn't allowed"
}

console.log(validate(json, Schema).isValid())
console.log(validate(json, Schema).firstError().message)