var net = require('net')

var server = net.createServer()
var sockets = []
server.on('connection', (socket) => {
    console.log('got a new connection')
    sockets.push(socket)
    socket.on('data', (data) => {
        console.log('got data:', data.toString())
        sockets.forEach((otherSocket) => {
            if (otherSocket !== socket) {
                otherSocket.write(data)
            }
        })

    });
})

server.on('error', (err) => {
    console.log('Server error:', err.message)
})

server.on('close', () => {
    console.log('connection closed')
    var index = sockets.indexOf(socket)
    sockets.splice(index, 1)
})
server.on('close', () => {
    console.log('Server closed')
})

server.listen(4000)