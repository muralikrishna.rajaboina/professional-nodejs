var net = require('net')
var port = 4000
var conn
(function connect(){
    conn = net.createConnection(port)
    conn.on('connect', ()=>{
        console.log('connected to server')
    })

    conn.on('error', (err)=>{
        console.log('connection got closed, will try to reconnect')
        connect()
    })

    conn.pipe(process.stdout, {end: false})
    process.stdin.pipe(conn)
}())