var net = require('net')
var port = 4000
var conn

var retryInterval = 3000
var retriedTimes = 0
var maxRetries = 10

process.stdin.resume()
process.stdin.on('data', (data) => {
    if (data.toString().trim().toLowerCase() === 'quit') {
        conn.end()
        process.stdin.pause()
    }
})
function connect(){
    function reconnect(){
        if(retriedTimes >= maxRetries){
            throw new Error('Max retries have been exceeded, I give up.')
        }
        retriedTimes += 1
        setTimeout(connect, retryInterval)
    }
    conn = net.createConnection(port)
    conn.on('connect', ()=>{
        retriedTimes = 0
        console.log('connected to server')
    })

    conn.on('error', (err)=>{
        console.log('Error in connection:', err)
    })

    conn.on('close', ()=>{
        console.log('connection got closed, will try to reconnect')
        reconnect()
    })

    process.stdin.pipe(conn, {end: false})
    conn.pipe(process.stdout, { end: false })

};
connect()