var net = require('net')
var port = 4000
var conn = net.createConnection(port, (conn)=>{
    console.log('We have a new connection')
})
conn.write("here is a string for you!",'utf8', ()=>{
    console.log('data was written out')
})

conn.on('data', (data)=>{
    console.log('some data has arrived:', data.toString())
})

conn.on('error', (err)=>{
    console.log('this error happened:' + err.message + 
    ', code: ' + err.code)
    // conn.end()
})