var users = require("../data/users");
var notLoggedIn = require("./middleware/not_logged_in");
var express = require("express");
var User = require("../data/models/user");
var router = express.Router();

router.get("/session/new", notLoggedIn, (req, res) => {
  res.render("session/new", { title: "Log in" });
});

router.post("/session", notLoggedIn, (req, res) => {
  User.findOne(
    { username: req.body.username, password: req.body.password },
    (err, user) => {
      if (err) {
        return next(err);
      }

      if (user) {
        req.session.user = user;
        res.redirect("/users");
      } else {
        res.redirect("/session/new");
      }
    }
  );
});

router.delete("/session", (req, res, next) => {
  req.session.destroy();
  res.redirect("/users");
});

module.exports = router;
