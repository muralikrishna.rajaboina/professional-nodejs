var http = require('http')
var path = require('path')
var fs = require('fs')
var options = {host: "localhost",
port: 4000,
path: "/sample.txt",
method: "GET"}

var outFile = path.normalize('./sample1.txt')
var file = fs.createWriteStream(outFile)

http.request(options, (res) =>{
    console.log('Got response: ' + res.statusCode)
    console.log('Got response: ', res)
    res.pipe(file)
    res.on('data', (data)=>{
        console.log(data.toString())
    })
}).end()