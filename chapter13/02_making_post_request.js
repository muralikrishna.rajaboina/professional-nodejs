var http = require('http')
var options = {
    host: "www.google.com",
    port: 80,
    path: "/upload",
    method:"POST"
}

var request = http.request(options, (response)=>{
    console.log('STATUS: ', response.statusCode)
    console.log('HEADERS: ', response.headers)
    response.setEncoding('utf8')
    response.on('data', (chunk)=>{
        console.log('Body: ', chunk)
    })
})

request.write('This is a piece of data. \n')
request.write('This is another piece of data. \n')
request.end()